package com.netcracker.yarotskas.utils;

import java.sql.Connection;

public interface TemplateDatabaseManager {

    Connection getConnection() throws Exception;

    void closeConnection(Connection connection);

}
