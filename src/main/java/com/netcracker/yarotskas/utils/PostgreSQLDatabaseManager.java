package com.netcracker.yarotskas.utils;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class PostgreSQLDatabaseManager implements TemplateDatabaseManager{

    public Connection getConnection() throws Exception {
        System.out.println("Connected to the PostgreSQL server successfully.");
        return DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/inventory", "postgres",
                "3353");
    }

    public void closeConnection(Connection connection) {
        if(connection !=null)
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }
}
