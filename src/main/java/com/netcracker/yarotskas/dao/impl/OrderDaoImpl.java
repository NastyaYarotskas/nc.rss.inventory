package com.netcracker.yarotskas.dao.impl;

import com.netcracker.yarotskas.dao.OrderDao;
import com.netcracker.yarotskas.entity.Order;
import com.netcracker.yarotskas.utils.PostgreSQLDatabaseManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl implements OrderDao {

    @Autowired
    private PostgreSQLDatabaseManager postgreSQLDatabaseManager;

    private final Connection connection;

    public OrderDaoImpl(final Connection connection) {
        this.connection = connection;
    }

    public List<Order> findAll() {
        List<Order> result = new ArrayList<Order>();

        try {
            PreparedStatement statement = connection.prepareStatement(SQLOrder.GET_ALL.QUERY);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                Order order = new Order();
                order.setId(rs.getLong("id"));
                order.setName(rs.getString("name"));
                order.setMaker(rs.getString("maker"));
                order.setCountryOfOrigin(rs.getString("countryOforigin"));
                order.setStore(rs.getString("store"));
                order.setQuantity(rs.getInt("quantity"));

                result.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return result;
    }

    public Order create(Order order) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQLOrder.INSERT.QUERY);
            statement.setString(1, order.getName());
            statement.setString(2, order.getMaker());
            statement.setString(3, order.getCountryOfOrigin());
            statement.setString(4, order.getStore());
            statement.setInt(5, order.getQuantity());
            ResultSet rs = statement.executeQuery();
            rs.next();
            order.setId(rs.getLong(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    public Order findById(Long id) {
        Order result = new Order();

        try {
            PreparedStatement statement = connection.prepareStatement(SQLOrder.GET.QUERY);
            statement.setLong(1, id);
            final ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                result.setId(id);
                result.setName(rs.getString("name"));
                result.setMaker(rs.getString("maker"));
                result.setCountryOfOrigin(rs.getString("countryOforigin"));
                result.setStore(rs.getString("store"));
                result.setQuantity(rs.getInt("quantity"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public Order update(Order order) {
        Order result = new Order();
        try {
            PreparedStatement statement = connection.prepareStatement(SQLOrder.UPDATE.QUERY);
            statement.setString(1, order.getName());
            statement.setString(2, order.getMaker());
            statement.setString(3, order.getCountryOfOrigin());
            statement.setString(4, order.getStore());
            statement.setInt(5, order.getQuantity());
            statement.setInt(6, order.getId().intValue());
            final ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Long id = rs.getLong("id");
                result = findById(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean deleteById(Long id) {
        boolean result = false;

        try {
            PreparedStatement statement = connection.prepareStatement(SQLOrder.DELETE.QUERY);
            statement.setInt(1, id.intValue());
            result = statement.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    enum SQLOrder {
        GET("SELECT * FROM orders WHERE id = (?)"),
        INSERT("INSERT INTO orders (id, name, maker, countryOforigin, store, quantity) VALUES (DEFAULT, (?), (?), (?), (?), (?)) RETURNING id"),
        DELETE("DELETE FROM orders WHERE id = (?) RETURNING id"),
        UPDATE("UPDATE orders SET name = (?), maker = (?), countryOforigin = (?), store = (?), quantity = (?)  WHERE id = (?) RETURNING id"),
        GET_ALL("SELECT * FROM orders");

        String QUERY;

        SQLOrder(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
