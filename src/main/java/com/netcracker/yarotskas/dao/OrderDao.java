package com.netcracker.yarotskas.dao;

import com.netcracker.yarotskas.entity.Order;

import java.util.List;

public interface OrderDao {

    List<Order> findAll();

    Order create(Order order);

    Order findById(Long id);

    Order update(Order order);

    boolean deleteById(Long id);
}
