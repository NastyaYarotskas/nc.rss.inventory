package com.netcracker.yarotskas.entity;

import lombok.Data;

@Data
public class Order {

    private Long id;

    private String name;

    private String nameOfProduct;

    private String maker;

    private String countryOfOrigin;

    private String store;

    private Integer quantity;

}
