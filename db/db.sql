CREATE TABLE IF NOT EXISTS orders (
  id                SERIAL PRIMARY KEY,
  name              VARCHAR(10) NOT NULL,
  maker             VARCHAR(10) NOT NULL,
  countryOforigin   VARCHAR(10) NOT NULL,
  store             VARCHAR(10) NOT NULL,
  quantity          INTEGER     NOT NULL
);

INSERT INTO orders (id, name, maker, countryOforigin, store, quantity)
VALUES (DEFAULT, 'name1', 'maker1', 'country1', 'store1', 10);
INSERT INTO orders (id, name, maker, countryOforigin, store, quantity)
VALUES (DEFAULT, 'name2', 'maker2', 'country2', 'store2', 20);